const fs = require('fs').promises;
const turf = require('@turf/turf');
const path = require('path');

(async () => {
  let dataDirectory = './public/data/json';

  let areas = [];
  try {
    const dataFiles = await fs.readdir(dataDirectory);
    for (const f of dataFiles) {
      if (f.substring(0,1) != '.' && f != 'areas.json') {
        const areaCode = f.slice(0, -8);
        console.log(`Parsing "${areaCode}"`);
        const content = JSON.parse(await fs.readFile(path.join(dataDirectory, f)));
        for (const i in content.features) {
          let feature = content.features[i];
          if (feature.geometry.type == 'GeometryCollection') {
            console.log(feature, feature.geometry.geometries);
            content.features[i] = turf.union(...(feature.geometry.geometries.map(g => turf.polygon(g.coordinates))));
          }
        }
        if (areaCode == 'BS') {
          console.log(content.features[1]);
        }
        const area = turf.union(...(content.features));
        area.properties.name = areaCode;
        areas.push(area);
      }
    }
  } catch (e) {
    console.log(e);
  }
  await fs.writeFile(path.join(dataDirectory, 'areas.json'), JSON.stringify(turf.featureCollection(areas)));
})();