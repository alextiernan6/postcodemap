(function (factory, window) {

    // define an AMD module that relies on 'leaflet'
    if (typeof define === 'function' && define.amd) {
        define(['leaflet'], factory);

    // define a Common JS module that relies on 'leaflet'
    } else if (typeof exports === 'object') {
        module.exports = factory(require('leaflet'));
    }

    // attach your plugin to the global 'L' variable
    if (typeof window !== 'undefined' && window.L) {
        window.L.PostcodeLayer = factory(L);
        window.L.postcodeLayer = function() {
            return new window.L.PostcodeLayer()
        };
    }
}(function (L) {
    return L.Layer.extend({
        onAdd: function (map) {
            let lastZoom = map.getZoom();

            let currentRegionLayer = null;
            var ukLayer = new L.geoJson.ajax("data/json/areas.json", {
              onEachFeature: (feature, layer) => {
                layer.on('click', (e) => {
                  selectRegion(feature.properties.name, !feature.properties.hasAllSelected);
                });
                const area = L.GeometryUtil.geodesicArea(feature.geometry.type == 'Polygon' ? layer.getLatLngs()[0] : layer.getLatLngs()[0][0]);
                layer.minZoom = 8 - Math.floor(Math.sqrt(area / 100000000));
                layer.bindTooltip(feature.properties.name, {
                  permanent: true,
                  className: 'regionTooltip',
                  direction:'center',
                });
              },
              style: baseStyle,
            });
            ukLayer.on('data:loaded', toggleLabels);
            ukLayer.on('mouseover', (e) => e.layer.setStyle({fillColor: highlighedColor, fillPattern: null}));
            ukLayer.on('mouseout', (e) => {
              let color = baseStyle.fillColor;
              if (e.layer.feature.properties.hasAllSelected) {
                e.layer.setStyle({fillColor: selectedColor, fillPattern: null});
              } else if (e.layer.feature.properties.hasSomeSelected) {
                e.layer.setStyle({fillColor: selectedColor, fillPattern: stripes});
              } else {
                e.layer.setStyle({fillColor: deactiveColor, fillPattern: null});
              }
            });
            ukLayer.on('contextmenu', (e) => {
              e.layer.bringToFront();
              lastZoom = map.getZoom();
              loadRegion(e.layer.feature.properties.name);
              setLayerStyles();
            });
            ukLayer.addTo(map);
      
            function loadRegion(region) {
              if (currentRegionLayer) {
                map.removeLayer(currentRegionLayer);
              }
              
              setLayerStyles();
              currentRegionLayer = new L.geoJson.ajax(`data/json/${region}.geojson`, {
                onEachFeature: (feature, layer) => {
                  feature.properties.region = region;
                  const outcode = feature.properties.name;
                  layer.on('click', onOutcodeClicked);
                  layer.on('contextmenu', (e) => {
                    map.removeLayer(currentRegionLayer);
                    map.setZoom(lastZoom);
                    currentRegionLayer = null;
                    setLayerStyles();
                  });
                  layer.bindTooltip(outcode, {sticky: true});
                  layer.setStyle({...baseStyle, fillColor: selectedAreas.has(outcode) ? selectedColor : baseStyle.fillColor});
                },
              });
              currentRegionLayer.region = region;
              currentRegionLayer.addTo(map);
              currentRegionLayer.on('data:loaded', (e) => map.fitBounds(e.target.getBounds()));
            }
      
            map.on('zoomend', toggleLabels);
            map.on('regionSelected', (layer) => {
                selectedAreas.add(layer.feature.properties.name);
                updateOutcodeStyle(layer);
            });
            map.on('regionDeselected', (layer) => {
                selectedAreas.delete(layer.feature.properties.name);
                updateOutcodeStyle(layer);
            });
      
            function toggleLabels() {
              const zoom = map.getZoom();
              map.eachLayer((layer) => {
                if (layer.minZoom) {
                  if (layer.minZoom < zoom) {
                    if (!layer.isTooltipOpen()) {
                      map.addLayer(layer.getTooltip());
                    }
                  } else {
                    map.closeTooltip(layer.getTooltip());
                  }
                }
              });
            }
      
            function onOutcodeClicked(e) {
              const outcode = e.target.feature.properties.name;
              if (selectedAreas.has(outcode)) {
                selectedAreas.delete(outcode);
              } else {
                selectedAreas.add(outcode);
              }
              updateOutcodeStyle(e.target);
      
              const regionLayers = currentRegionLayer.getLayers();
              const selectedCount = regionLayers.filter((l) => selectedAreas.has(l.feature.properties.name)).length;
              setRegionSelected(e.target.feature.properties.region, regionLayers.length == selectedCount, selectedCount > 0);
            }
      
            function updateOutcodeStyle(layer) {
              if (selectedAreas.has(layer.feature.properties.name)) {
                layer.setStyle({fillColor: selectedColor});
              } else {
                layer.setStyle({fillColor: baseStyle.fillColor});
              }
            }
      
            function selectRegion(region, selected = true) {
              let onEachFeature = (feature, layer) => selectedAreas.add(feature.properties.name);
              if (!selected) {
                onEachFeature = (feature, layer) => selectedAreas.delete(feature.properties.name);
              }
      
              L.geoJson.ajax(`data/json/${region}.geojson`, {onEachFeature});
              setRegionSelected(region, selected);
            }
      
            function setRegionSelected(region, value, hasSomeSelected) {
              map.eachLayer((l) => {
                if (l.feature && l.feature.properties.name == region) {
                  l.feature.properties.hasAllSelected = value;
                  l.feature.properties.hasSomeSelected = hasSomeSelected;
                }
              });
            }
      
            function setLayerStyles() {
              ukLayer.eachLayer((l) => {
                l.setStyle({
                  weight: (currentRegionLayer && currentRegionLayer.region == l.feature.properties.name) ? 5 : 1,
                  fillColor: l.feature.properties.hasAllSelected ? selectedColor : deactiveColor,
                  fillPattern: l.feature.properties.hasSomeSelected ? stripes : null,
                })
              });
            }
        }
    });
}, window));