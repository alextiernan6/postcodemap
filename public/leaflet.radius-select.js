(function (factory, window) {

    // define an AMD module that relies on 'leaflet'
    if (typeof define === 'function' && define.amd) {
        define(['leaflet'], factory);

    // define a Common JS module that relies on 'leaflet'
    } else if (typeof exports === 'object') {
        module.exports = factory(require('leaflet'));
    }

    // attach your plugin to the global 'L' variable
    if (typeof window !== 'undefined' && window.L) {
        window.L.Control.RadiusSelect = factory(L);
        window.L.control.radiusSelect = function() {
            return new window.L.Control.RadiusSelect()
        };
    }
}(function (L) {
    var RadiusSelect = L.Control.extend({
        options: {
            position: 'topleft',
        },
        onAdd: function (map) {
            let centerMarker = null;
            let radiusMarker = null;
            let selectedCircle = null;

            function onButtonClick(e) {
                function closeRadiusSelect() {
                    map.removeLayer(centerMarker);
                    map.removeLayer(radiusMarker);
                    map.removeLayer(selectedCircle);
                    centerMarker = null;
                    radiusMarker = null;
                    selectedCircle = null;
                }
                
                centerMarker = L.marker(map.mouseEventToLatLng(e), {icon: L.divIcon({html: '', iconSize: null, className: 'radius-select-marker'})})
                                .bindTooltip('Click to place the center of the circle', {direction: 'top',offset: [0, -10]})
                                .openTooltip();
                centerMarker.on('click', (e) => {
                const center = centerMarker.getLatLng();
                selectedCircle = L.circle(center, {radius: 1}).addTo(map);
                radiusMarker = L.marker(center, {icon: L.divIcon({html: '', iconSize: null, className: 'radius-select-marker'})})
                                .bindTooltip('Click to finish the circle<br/>Right click to cancel', {direction: 'top',offset: [0, -10]})
                                .openTooltip();
                radiusMarker.on('contextmenu', closeRadiusSelect)
                radiusMarker.on('click', closeRadiusSelect);
                radiusMarker.addTo(map);
                });
                centerMarker.addTo(map);
            }

            const container = L.DomUtil.create('div', 'leaflet-bar');
            L.DomEvent
                .addListener(container, 'click', L.DomEvent.stopPropagation)
                .addListener(container, 'click', L.DomEvent.preventDefault)
                .addListener(container, 'click', onButtonClick);
    
            map.on('mousemove', (e) => {
                if (centerMarker && !radiusMarker) {
                    centerMarker.setLatLng(e.latlng);
                } else if (radiusMarker) {
                    radiusMarker.setLatLng(e.latlng);
                    const center = centerMarker.getLatLng();
                    const distance = map.distance(e.latlng, center);
                    selectedCircle.setRadius(distance);
                    const arc = turf.circle([center.lng, center.lat], Math.max(distance / 1000, 1));
                    map.eachLayer((l) => {
                        if (l.feature && l.feature.geometry.type == 'Polygon' && l.feature.properties.region) {
                            if (turf.booleanContains(arc, l.feature)) {
                                map.fire('regionSelected', l);
                            } else {
                                map.fire('regionDeselected', l);
                            }
                        }
                    });
                }
            });
    
            const radiusSelectButton = L.DomUtil.create('a', 'radius-select-btn', container);
            radiusSelectButton.title = 'Radius Select';
            radiusSelectButton.href = '#';
            return container;
        }
    });
    return RadiusSelect;
}, window));